import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import inject from "@rollup/plugin-inject";
import path from "path";

import { resolve, dirname } from "node:path";
import { fileURLToPath } from "url";
import VueI18nPlugin from "@intlify/unplugin-vue-i18n/vite";

export default defineConfig({
  plugins: [
    vue(),
    inject({
      emailjs: "@emailjs/browser",
      Toastify: "toastify-js",
      Atropos: "atropos",
      include: "src/**/*.vue",
    }),
    VueI18nPlugin({
      include: resolve(
        dirname(fileURLToPath(import.meta.url)),
        " ./src/i18n/locations/**"
      ),
    }),
  ],
  server: {
    port: 2000,
  },
  resolve: {
    alias: {
      "@": path.resolve("src"),
    },
  },
});
