import { createRouter, createWebHashHistory } from "vue-router";
import Layout from "../views/Layout.vue";

const routes = [
  {
    path: "/",
    component: Layout,
    name: "Layout",
  },
  {
    path: "/:pathMatch(.*)*",
    redirect: "/",
  },
];

const history = createWebHashHistory();

const router = createRouter({
  history,
  routes,
});

export default router;
