import IconPostgreSQL from "../../assets/img/technologies/postgresql.svg";
import IconVue from "../../assets/img/technologies/vue.svg";
import IconCss from "../../assets/img/technologies/css.svg";
import IconVite from "../../assets/img/technologies/vite.svg";
import IconSass from "../../assets/img/technologies/sass.svg";
import IconBootstrap from "../../assets/img/technologies/bootstrap.svg";
import IconJavascript from "../../assets/img/technologies/javascript.svg";
import IconNodeJS from "../../assets/img/technologies/nodejs.svg";
import IconRails from "../../assets/img/technologies/rails.svg";

export const SKILLS = [
  {
    id: 1,
    src: IconPostgreSQL,
    alt: "Icon PostgreSQL",
    title: "PostgreSQL",
  },
  {
    id: 2,
    src: IconVue,
    alt: "Icon Vue",
    title: "Vue",
  },
  {
    id: 3,
    src: IconRails,
    alt: "Icon Rails",
    title: "Rails",
  },
  {
    id: 4,
    src: IconNodeJS,
    alt: "Icon NodeJS",
    title: "Node.JS",
  },
  {
    id: 5,
    src: IconVite,
    alt: "Icon Vite",
    title: "Vite",
  },
  {
    id: 6,
    src: IconBootstrap,
    alt: "Icon Bootstrap",
    title: "Bootstrap",
  },
  {
    id: 7,
    src: IconJavascript,
    alt: "Icon Javascript",
    title: "Javascript",
  },
  {
    id: 8,
    src: IconSass,
    alt: "Icon Sass",
    title: "Sass",
  },
  {
    id: 9,
    src: IconCss,
    alt: "Icon Css",
    title: "CSS",
  },
];
