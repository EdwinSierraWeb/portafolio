import { THEMES } from "../JSON/json";

const setLocalTheme = (theme) => {
  localStorage.setItem("theme", theme);
};

const getLocalTheme = () => {
  return localStorage.getItem("theme");
};

const setTheme = (theme) => {
  setLocalTheme(theme);

  // if (theme === THEMES.LIGHT) {
  //   setNewClass("bg-dark", "bg-light");
  //   setNewClass("text-light", "text-dark");
  //   setNewClass("btn-dark", "btn-light");
  //   setNewClass("shadow-light-sm", "shadow-dark-sm");
  // } else {
  //   setNewClass("bg-light", "bg-dark");
  //   setNewClass("text-dark", "text-light");
  //   setNewClass("btn-light", "btn-dark");
  //   setNewClass("shadow-dark-sm", "shadow-light-sm");
  // }
};

const setNewClass = (last_class, new_class) => {
  let elements = document.querySelectorAll(`.${last_class}`);

  elements.forEach((element) => {
    element.classList.remove(last_class);

    element.classList.add(new_class);
  });
};

const setDefaultTheme = () => {
  const theme = getLocalTheme();

  if (!theme) {
    setTheme(THEMES.LIGHT);
  } else {
    setTheme(theme);
  }
};

export { setLocalTheme, getLocalTheme, setDefaultTheme, setTheme };
