export const getScreenPixelsByPercentage = (percentage) => {
  return window.innerHeight * (percentage / 100);
};
