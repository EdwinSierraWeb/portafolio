import * as THREE from "three";
import { CSS3DRenderer } from "three/addons/renderers/CSS3DRenderer.js";
import { RectAreaLightHelper } from "three/addons/helpers/RectAreaLightHelper.js";
import tilesImage from "../../../assets/img/textures/Wood_Wall_003_basecolor.jpg";
const textureLoader = new THREE.TextureLoader();

const extrudeSettings = {
  depth: 8,
  bevelEnabled: true,
  bevelSegments: 32,
  steps: 1,
  bevelSize: 1,
  bevelThickness: 2,
  curveSegments: 32,
};

export const getScene = () => {
  const scene = new THREE.Scene();
  scene.background = new THREE.Color(0x000000);

  return scene;
};

export const getCamera = () => {
  const camera = new THREE.PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    0.1,
    1500
  );

  camera.position.set(-85, 20, 0);
  camera.rotation.set(0, -Math.PI / 2, 0);
  // camera.position.set(10, 34.5, 54.95);
  // camera.rotation.set(0, 0, -Math.PI / 2);
  // camera.position.set(32, 38, -29.6);
  // camera.rotation.set(0.1, -Math.PI / 2, 0);

  return camera;
};

export const getRenderer = () => {
  const renderer = new THREE.WebGLRenderer({ antialias: true });
  const room = document.getElementById("room");
  const width = room.clientWidth;
  const height = room.clientHeight;

  renderer.setSize(width, height);
  room.appendChild(renderer.domElement);
  renderer.setPixelRatio(window.devicePixelRatio);

  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = THREE.PCFSoftShadowMap;

  return renderer;
};

export const getCSS3DRenderer = () => {
  let cssRenderer = new CSS3DRenderer();
  cssRenderer.setSize(window.innerWidth, window.innerHeight);
  cssRenderer.domElement.style.position = "absolute";
  cssRenderer.domElement.style.top = 0;
  document.body.appendChild(cssRenderer.domElement);

  return cssRenderer;
};

export const getLights = () => {
  const light = new THREE.DirectionalLight(0xffffff, 1);
  light.position.set(-100, 100, 0);
  light.castShadow = true;

  light.shadow.camera.left = -100;
  light.shadow.camera.right = 100;
  light.shadow.camera.top = 100;
  light.shadow.camera.bottom = -100;

  return light;
};

const setRemainingOptions = (mesh, { position, rotation, shadow }) => {
  if (position) {
    mesh.position.set(position.x, position.y, position.z);
  }

  if (rotation) {
    mesh.rotation.set(rotation.x, rotation.y, rotation.z);
  }

  if (shadow) {
    mesh.castShadow = shadow.castShadow;
    mesh.receiveShadow = shadow.receiveShadow;
  }
};

export const getBoxMesh = ({ size, options, position, shadow, rotation }) => {
  const geometry = new THREE.BoxGeometry(size.x, size.y, size.z);
  // const material = new THREE.MeshStandardMaterial(options);
  const material = new THREE.MeshStandardMaterial(options);
  const mesh = new THREE.Mesh(geometry, material);

  setRemainingOptions(mesh, { position, rotation, shadow });

  return mesh;
};

export const getCylinder = ({
  cylindrerOptions,
  options,
  position,
  shadow,
  rotation,
}) => {
  const geometry = new THREE.CylinderGeometry(
    cylindrerOptions.radiusTop,
    cylindrerOptions.radiusBottom,
    cylindrerOptions.height,
    cylindrerOptions.segments,
    1,
    cylindrerOptions.openEnded
  );
  const material = new THREE.MeshStandardMaterial(options);
  const mesh = new THREE.Mesh(geometry, material);

  setRemainingOptions(mesh, { position, rotation, shadow });

  return mesh;
};

export const getCircle = ({
  circleOptions,
  options,
  position,
  shadow,
  rotation,
}) => {
  const geometry = new THREE.CircleGeometry(
    circleOptions.radius,
    circleOptions.segments,
    0,
    Math.PI * 2
  );
  const material = new THREE.MeshStandardMaterial(options);
  const mesh = new THREE.Mesh(geometry, material);

  setRemainingOptions(mesh, { position, rotation, shadow });

  return mesh;
};

export const getRing = ({
  ringOptions,
  options,
  position,
  shadow,
  rotation,
}) => {
  const geometry = new THREE.RingGeometry(
    ringOptions.innerRadius,
    ringOptions.outerRadius,
    ringOptions.thetaSegments,
    1,
    0,
    Math.PI * 2
  );
  const material = new THREE.MeshStandardMaterial(options);
  const mesh = new THREE.Mesh(geometry, material);

  setRemainingOptions(mesh, { position, rotation, shadow });

  return mesh;
};

export const getTorus = ({
  torusOptions,
  options,
  position,
  shadow,
  rotation,
}) => {
  const geometry = new THREE.TorusGeometry(
    torusOptions.radius,
    torusOptions.tubeWidth,
    torusOptions.radialSegments,
    torusOptions.tubularSegments,
    Math.PI * 2
  );
  const material = new THREE.MeshStandardMaterial(options);
  const mesh = new THREE.Mesh(geometry, material);

  setRemainingOptions(mesh, { position, rotation, shadow });

  return mesh;
};

export const addShape = (
  { scale, position, options, rotation, shadow, bevelEnabled },
  shape
) => {
  const updatedExtrudeSettings = { ...extrudeSettings };
  if (typeof bevelEnabled == "boolean") {
    updatedExtrudeSettings.bevelEnabled = bevelEnabled;
  }

  if (options.map) {
    const woodTexture = textureLoader.load(tilesImage);

    options.map = null;
    options.map = woodTexture;

    options.map.wrapS = options.map.wrapT = THREE.RepeatWrapping;
    options.map.offset.set(0, 0.5);
    options.map.repeat.set(0.005, 0.05);
  }

  const geometry = new THREE.ExtrudeGeometry(shape, updatedExtrudeSettings);
  const material = new THREE.MeshStandardMaterial(options);
  const mesh = new THREE.Mesh(geometry, material);

  mesh.scale.set(scale.x, scale.y, scale.z);
  setRemainingOptions(mesh, { position, rotation, shadow });

  return mesh;
};

export const getSphere = ({
  sphereOptions,
  position,
  options,
  rotation,
  shadow,
}) => {
  const geometry = new THREE.SphereGeometry(
    sphereOptions.radius,
    sphereOptions.widthSegments,
    sphereOptions.heightSegments,
    0,
    Math.PI * 2,
    0,
    sphereOptions.thetaLength
  );
  const material = new THREE.MeshStandardMaterial(options);
  material.side = THREE.DoubleSide;

  const mesh = new THREE.Mesh(geometry, material);

  setRemainingOptions(mesh, { position, rotation, shadow });

  return mesh;
};

export const getDodecahedron = ({
  geometyOptions,
  position,
  options,
  rotation,
  shadow,
}) => {
  const geometry = new THREE.DodecahedronGeometry(
    geometyOptions.radius,
    geometyOptions.detail
  );
  const material = new THREE.MeshStandardMaterial(options);
  const mesh = new THREE.Mesh(geometry, material);

  setRemainingOptions(mesh, { position, rotation, shadow });

  return mesh;
};

export const getRoundedBoxShape = (x, y, rx, ry) => {
  const shape = new THREE.Shape()
    .lineTo(x, 0)
    .quadraticCurveTo(x + rx, 0, x + rx, ry)
    .lineTo(x + rx, y + ry)
    .quadraticCurveTo(x + rx, y + ry + rx, x, y + ry + rx)
    .lineTo(0, y + ry + rx)
    .quadraticCurveTo(-rx, y + ry + rx, -rx, y + ry)
    .lineTo(-rx, ry)
    .quadraticCurveTo(-rx, 0, 0, 0);

  return shape;
};

export const getRectLight = ({ scale, position, options, rotation }) => {
  const rectLight = new THREE.RectAreaLight(
    options.color,
    options.intensity,
    scale.x,
    scale.y
  );

  setRemainingOptions(rectLight, { position, rotation });

  const rectLightHelper = new RectAreaLightHelper(rectLight);
  rectLight.add(rectLightHelper);

  return rectLight;
};
