import header from "./header";
import home from "./home";
import contact from "./contact";
import experience from "./experience";
import about from "./about";
import skills from "./skills";
import others from "./others";
import popup from "./popup";

export default {
  header: header,
  home: home,
  experience: experience,
  contact: contact,
  about: about,
  skills: skills,
  others: others,
  popup: popup,
  locale: {
    en: "Inglés",
    es: "Español",
  },
};
