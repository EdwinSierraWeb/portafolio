export default {
  mainInfo:
    "Buscador de soluciones diversas, eficientes, inovadoras y con valor a largo plazo , siempre orientadas al cliente. Con experiencia en desarrollo web, amador de tecnologia y motivato a crear impacto a donde quiera que vaya.",
  otherThings: "Otras cosas sobre mí: ",
  thing1: "Tengo 20 años.",
  thing2: "He estado trabajando en este campo desde el 2022.",
  thing3: "Nunca paro de aprender y esforzarme.",
  thing4: "Todo lo que hago, lo analizo y planeo.",
  thing5: "Me considero una buena persona.",
};
