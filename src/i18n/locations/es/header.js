export default {
  experience: "Experiencia",
  contact: "Contactar",
  about: "Sobre mí",
  skills: "Habilidades",
  others: "Otros",
};
