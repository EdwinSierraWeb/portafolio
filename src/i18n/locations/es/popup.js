export default {
  success: "Enviado correctamente!",
  failed: "Hubo un error enviando el email...",
  validate: {
    email: "El campo Correo debe de ser un correo valido.",
  },
};
