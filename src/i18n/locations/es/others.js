export default {
  languages: "IDIOMAS",
  language_details: "Español e Inglés",
  education: "EDUCACIÓN",
  education_details: "Autodidacta. Universidad en curso.",
  downloadCVLabel: "Descargar CV",
};
