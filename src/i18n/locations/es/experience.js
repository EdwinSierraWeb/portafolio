export default {
  company:
    "Ingeniero de Software en Sicamex (08/2022 - Present) [Monterrey, México] [CRM - ERP, B2B]",
  responsabilities: "RESPONSABILIDADES",
  responsability_1: "Analizar y llevar a cabo los requisitos de software.",
  responsability_2:
    "Iniciativas de ingeniería para mejorar el rendimiento, reducir dependencias y mejorar la experiencia de desarrollo y operación.",
  responsability_3:
    "Apoyar a mis compañeros de trabajo con respecto a dudas de código y lógica de negocio.",
  responsability_4:
    "Promover mejores prácticas de ingeniería y calidad de software mediante ayuda de código, revisiones de código y dar feedback.",
  responsability_5: "Realizar desarrollo Front end y Back end.",
  responsability_6: "Desarrollador principal de Front end de la empresa.",
  responsability_7:
    "Desarrollador principal de Back end (NodeJs) de la empresa.",
  responsability_8: "Debugear.",
  responsability_9: "Probar la aplicación móvil.",

  key_accomplishments: "LOGROS CLAVE",
  key_accomplishment_1:
    "Ayudé a gestionar la información de la empresa desarrollando un proyecto de Back office (Front end).",
  key_accomplishment_2:
    "Evite la obsolescencia del Front-end al migrar de Vue 2 a Vue 3.",
  key_accomplishment_3:
    "Mejorar la eficiencia, efectividad y rendimiento al crear un módulo de Reingeniería de rutas.",
  key_accomplishment_4:
    "Mejorar los pagos de la empresa mediante la creación del módulo de Créditos y Cobranzas.",
  key_accomplishment_5:
    "Reducir informes obsoletos y aumentar el tiempo de descarga en un 96% al migrar de Java a NodeJs, como resultado, eliminamos la API de Java.",
  key_accomplishment_6:
    "Evite la obsolescencia de la API de un proyecto al ayudar a migrar de  Ruby  v2 a v3 y Rails v4 a v7.",
  key_accomplishment_7:
    "Mejorar la eficiencia de descarga de reportes y reducir la sobrecarga de datos gracias a las técnicas de streamlining.",
  key_accomplishment_8:
    "Mejore los componentes base al migrarlos a unos mejores.",
  key_accomplishment_9:
    "Evite la obsolescencia de mapas al migrarlos (Google Maps API).",
  key_accomplishment_10:
    "Mejorar la perspectiva de Liquidación para ver los Créditos y Cobranzas al manejarlos mediante visitas y pedidos.",
  key_accomplishment_11:
    "Aumentar la eficiencia de las rutas al crear indicadores de ruta con tiempos, ventas, productividad y servicios.",
  key_accomplishment_12:
    "Mejorar la eficiencia del repositorio al estandarizar los componentes.",
  key_accomplishment_13:
    "Habilitar indicador de Visitas, al crear módulo de Visitas.",
  key_accomplishment_14:
    "Otras creaciones, Causas de Declinación, Encuestas, Notificaciones y Tipos de Notificaciones.",
};
