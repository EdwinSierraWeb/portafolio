export default {
  mainInfo:
    "I am a seeker for diverse, lean, innovative and long-term solutions, always customer-focused. With a background on web development, technology lover and motivated to create impact wherever I go.",
  otherThings: "Other things about me: ",
  thing1: "I am 20 years old.",
  thing2: "I've been working at this field from 2022.",
  thing3: "I never stop learning and struggling.",
  thing4: "Everything I do I analyse and plan it.",
  thing5: "I consider myself a good person.",
};
