export default {
  company:
    "Software Engineer at SICAMEX (08/2022 - Present) [Monterrey, Mexico] [CRM - ERP, B2B]",
  responsabilities: "RESPONSIBILITIES",
  responsability_1: "Analyze and carry out software requirements.",
  responsability_2:
    "Engineering initiative to improve performance, reduce dependencies, enhance the development and operational experience.",
  responsability_3: "Support my coworker's at code doubts and business logic.",
  responsability_4:
    "Promote engineering  best practices and quality software by code helping, reviewing code and giving feedback.",
  responsability_5: "Carry out Front end and Back end development.",
  responsability_6: "Main company's Front end developer.",
  responsability_7: "Main company's Back end (NodeJs) developer.",
  responsability_8: "Debugging.",
  responsability_9: "Test mobile application.",

  key_accomplishments: "KEY ACCOMPLISHMENTS",
  key_accomplishment_1:
    "Helped to manage company's information by developing a Back office project (Front end).",
  key_accomplishment_2:
    "Avoided Front-end deprication by migrating from Vue 2 to Vue 3.",
  key_accomplishment_3:
    "Improved efficiency, effectiveness and performance by creating a Route Reengineering module.",
  key_accomplishment_4:
    "Improved the company's payments by creating Credits and Collections module.",
  key_accomplishment_5:
    "Reduced deprecated reports and increased 96% downloading time by migrating from Java to NodeJs, as a result, we removed Java's API.",
  key_accomplishment_6:
    "Avoided project's API  deprecation by helping to migrate from Ruby  v2 to v3 and Rails v4 to v7.",
  key_accomplishment_7:
    "Enhanced reports download efficiency and reduced data overhead achieved through Streamlining Techniques.",
  key_accomplishment_8:
    "Improved Base components efficiency by migrating them to better ones.",
  key_accomplishment_9:
    "Avoided map's deprecation by migrating them (Google Maps API).",
  key_accomplishment_10:
    "Improved Liquidation's points of view to see Credits and Collections by handling them by visits and orders.",
  key_accomplishment_11:
    "Increased route's efficiency by creating route indicators with times, sales, productivity and services.",
  key_accomplishment_12:
    "Increased repository efficiency by standardizing components.",
  key_accomplishment_13:
    "Enabled Visits indicator, by creating a Visits module.",
  key_accomplishment_14:
    "Other creations, Decline Causes, Surveys, Notifications and Notification Types.",
};
