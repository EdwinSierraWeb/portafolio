export default {
  experience: "Experience",
  contact: "Contact",
  about: "About me",
  skills: "Skills",
  others: "Others",
};
