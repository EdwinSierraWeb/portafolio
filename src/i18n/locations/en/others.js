export default {
  languages: "LANGUAGES",
  language_details: "Spanish and English",
  education: "EDUCATION",
  education_details: "Self taught. University in progress.",
  downloadCVLabel: "Download CV",
};
