export default {
  success: "Success!",
  failed: "Failed...",
  validate: {
    email: "The field Email must be a valid email.",
  },
};
