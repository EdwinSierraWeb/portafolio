import { createI18n } from "vue-i18n";
import en from "./locations/en/en";
import es from "./locations/es/es";

const messages = {
  en: en,
  es: es,
};

export default createI18n({
  locale: import.meta.env.VITE_DEFAULT_LOCALE,
  fallbackLocale: import.meta.env.VITE_FALLBACK_LOCALE,
  legacy: false,
  globalInjection: true,
  messages,
});
