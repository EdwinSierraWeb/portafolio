import { addShape, getRoundedBoxShape } from "../../shared/helpers/threejs";
import * as THREE from "three";

export const drawKeyboard = () => {
  const keyboardSupportOptions = {
    scale: { x: 1, y: 1, z: 0.034 },
    position: { x: -22.8, y: 12.62, z: -39.8 },
    options: { color: 0xffffff },
    rotation: { x: -Math.PI / 2 + 0.2, y: 0, z: 0 },
    shadow: { castShadow: true, receiveShadow: true },
  };
  const keyboardSupportShape = getRoundedBoxShape(5.1, 1.4, 0.05, 0.05);
  const keyboardSupport = addShape(
    keyboardSupportOptions,
    keyboardSupportShape
  );

  //bottom line
  const keyOptions = {
    scale: { x: 0.8, y: 0.8, z: 0.005 },
    position: { x: -23.3, y: 12.82, z: -39.2 },
    options: { color: 0xffffff },
    rotation: { x: -Math.PI / 2 + 0.23, y: 0, z: 0 },
    bevelEnabled: false,
    shadow: { castShadow: false, receiveShadow: true },
  };
  const keyShape = getRoundedBoxShape(0.35, 0.35, 0.035, 0.035);
  const key = addShape(keyOptions, keyShape);

  keyOptions.position.x = -22.85;
  const key2 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -22.4;
  const key3 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -21.95;
  const key4 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -19.25;
  const key5 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -18.8;
  const key6 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -18.35;
  const key7 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -17.45;
  const key8 = addShape(keyOptions, keyShape);

  //space
  keyOptions.position.x = -21.35;
  keyOptions.scale.x = 5.08;
  const spaceKey = addShape(keyOptions, keyShape);

  //arrow
  keyOptions.position.x = -17.9;
  keyOptions.scale.y = -0.37;
  keyOptions.scale.x = 0.8;

  keyOptions.position.y = 12.898;
  keyOptions.position.z = -39.526;
  const arrowUpKey = addShape(keyOptions, keyShape);

  keyOptions.position.y = 12.858;
  keyOptions.position.z = -39.35;
  const arrowDownKey = addShape(keyOptions, keyShape);

  //nextline
  keyOptions.scale.y = 0.8;
  keyOptions.position.z = -39.65;
  keyOptions.position.y = 12.918;
  keyOptions.scale.x = 0.8;

  keyOptions.position.x = -22.4;
  const key9 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -21.95;
  const key10 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -21.5;
  const key11 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -21.05;
  const key12 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -20.6;
  const key13 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -20.15;
  const key14 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -19.7;
  const key15 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -19.25;
  const key16 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -18.8;
  const key17 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -18.35;
  const key18 = addShape(keyOptions, keyShape);

  //shift
  keyOptions.position.x = -23.265;
  keyOptions.scale.x = 1.87;
  const startShift = addShape(keyOptions, keyShape);

  keyOptions.position.x = -17.86;
  const endShift = addShape(keyOptions, keyShape);

  //next line
  keyOptions.position.z = -40.1;
  keyOptions.position.y = 13.009;
  keyOptions.scale.x = 0.8;

  keyOptions.position.x = -22.7;
  const key19 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -22.25;
  const key20 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -21.8;
  const key21 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -21.35;
  const key22 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -20.9;
  const key23 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -20.45;
  const key24 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -20;
  const key25 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -19.55;
  const key26 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -19.1;
  const key27 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -18.65;
  const key28 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -18.2;
  const key29 = addShape(keyOptions, keyShape);

  //caption lock
  keyOptions.position.x = -23.29;
  keyOptions.scale.x = 1.2;
  const captionsLock = addShape(keyOptions, keyShape);

  //enter
  keyOptions.position.x = -17.725;
  keyOptions.scale.x = 1.52;
  const enter = addShape(keyOptions, keyShape);

  //next line
  keyOptions.position.z = -40.55;
  keyOptions.position.y = 13.1;
  keyOptions.scale.x = 0.8;

  keyOptions.position.x = -23.3;
  const key30 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -22.85;
  const key31 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -22.4;
  const key32 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -21.95;
  const key33 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -21.5;
  const key34 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -21.05;
  const key35 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -20.6;
  const key36 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -20.15;
  const key37 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -19.7;
  const key38 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -19.25;
  const key39 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -18.8;
  const key40 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -18.35;
  const key41 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -17.9;
  const key42 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -17.45;
  const key43 = addShape(keyOptions, keyShape);

  //next line
  keyOptions.position.z = -41;
  keyOptions.position.y = 13.191;
  keyOptions.scale.x = 0.8;

  keyOptions.position.x = -23.04;
  const key44 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -22.59;
  const key45 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -22.14;
  const key46 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -21.69;
  const key47 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -21.24;
  const key48 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -20.79;
  const key49 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -20.34;
  const key50 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -19.89;
  const key51 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -19.44;
  const key52 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -18.99;
  const key53 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -18.54;
  const key54 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -18.09;
  const key55 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -23.315;
  keyOptions.scale.x = 0.37;
  const keySymbol = addShape(keyOptions, keyShape);

  keyOptions.scale.x = 1.23;
  keyOptions.position.x = -17.62;
  const remove = addShape(keyOptions, keyShape);

  //next line
  keyOptions.position.z = -41.45;
  keyOptions.position.y = 13.282;
  keyOptions.scale.x = 0.8;

  keyOptions.position.x = -23.3;
  const key56 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -22.85;
  const key57 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -22.4;
  const key58 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -21.95;
  const key59 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -21.5;
  const key60 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -21.05;
  const key61 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -20.6;
  const key62 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -20.15;
  const key63 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -19.7;
  const key64 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -19.25;
  const key65 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -18.8;
  const key66 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -18.35;
  const key67 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -17.9;
  const key68 = addShape(keyOptions, keyShape);

  keyOptions.position.x = -17.45;
  const key69 = addShape(keyOptions, keyShape);

  const group = new THREE.Group();
  group.add(
    keyboardSupport,
    key,
    key2,
    key3,
    key4,
    key5,
    key6,
    key7,
    key8,
    key9,
    key10,
    key11,
    key12,
    key13,
    key14,
    key15,
    key16,
    key17,
    key18,
    key19,
    key20,
    key21,
    key22,
    key23,
    key24,
    key25,
    key26,
    key27,
    key28,
    key29,
    key30,
    key31,
    key32,
    key33,
    key34,
    key35,
    key36,
    key37,
    key38,
    key39,
    key40,
    key41,
    key42,
    key43,
    key44,
    key45,
    key46,
    key47,
    key48,
    key49,
    key50,
    key51,
    key52,
    key53,
    key54,
    key55,
    key56,
    key57,
    key58,
    key59,
    key60,
    key61,
    key62,
    key63,
    key64,
    key65,
    key66,
    key67,
    key68,
    key69,
    spaceKey,
    arrowUpKey,
    arrowDownKey,
    startShift,
    endShift,
    captionsLock,
    enter,
    keySymbol,
    remove
  );

  return group;
};
