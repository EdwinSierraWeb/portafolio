import { getBoxMesh } from "../../shared/helpers/threejs";
import { tilesTexture, marbleTexture } from "./textures.js";
import * as THREE from "three";

export const drawWalls = () => {
  const floorOptions = {
    size: { x: 350, y: 0.5, z: 350 },
    position: { x: 0, y: 0, z: 0 },
    options: { map: marbleTexture, roughness: 0, metalness: 0 },
    shadow: { castShadow: false, receiveShadow: true },
  };
  const floor = getBoxMesh(floorOptions);

  // const wallOption = {
  //   size: { x: 100, y: 54, z: 2 },
  //   position: { x: 0, y: 27, z: -51 },
  //   options: { map: tilesTexture },
  //   shadow: { castShadow: false, receiveShadow: true },
  // };
  // const wall = getBoxMesh(wallOption);

  // wallOption.position.z = 51;
  // const wall4 = getBoxMesh(wallOption);

  // const wallOption2 = {
  //   size: { x: 2, y: 50, z: 100 },
  //   position: { x: -51, y: 4, z: 0 },
  //   options: { map: tilesTexture },
  //   shadow: { castShadow: false, receiveShadow: true },
  // };

  // const wall3 = getBoxMesh(wallOption2);

  const group = new THREE.Group();
  group.add(floor);
  // group.add(floor, wall, wall4);

  return group;
};
