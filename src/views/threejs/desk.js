import { addShape, getCylinder } from "../../shared/helpers/threejs";
import * as THREE from "three";

export const drawDesk = () => {
  const topSupportOptions = {
    scale: { x: 13, y: 11, z: 0.3 },
    position: { x: -48.7, y: 10, z: -23.6 },
    options: { color: 0xffffff },
    rotation: { x: -Math.PI / 2, y: 0, z: 0 },
    bevelEnabled: false,
    shadow: { castShadow: true, receiveShadow: true },
  };
  const topSupportShape = new THREE.Shape()
    .lineTo(1, 0)
    .quadraticCurveTo(1.1, 0, 1.1, 0.1)
    .lineTo(1.1, 1.1)
    .quadraticCurveTo(1.1, 1.2, 1.2, 1.2)
    .lineTo(3.2, 1.2)
    .quadraticCurveTo(3.3, 1.2, 3.3, 1.3)
    .lineTo(3.3, 2.3)
    .quadraticCurveTo(3.3, 2.4, 3.2, 2.4)
    .lineTo(0, 2.4)
    .quadraticCurveTo(-0.1, 2.4, -0.1, 2.3)
    .lineTo(-0.1, 0.1)
    .quadraticCurveTo(-0.1, 0, 0, 0);

  const topSupport = addShape(topSupportOptions, topSupportShape);

  const legOptions = {
    cylindrerOptions: {
      radiusTop: 1.4,
      radiusBottom: 1.7,
      height: 9.8,
      segments: 32,
    },
    position: { x: -47, y: 5.1, z: -27 },
    options: { color: 0xffffff },
    shadow: { castShadow: true, receiveShadow: true },
  };
  const leg = getCylinder(legOptions);

  legOptions.position.x = -38;
  const leg2 = getCylinder(legOptions);

  legOptions.position.x = -47;
  legOptions.position.z = -47;
  const middleLeg = getCylinder(legOptions);

  legOptions.position.x = -9;
  const leg4 = getCylinder(legOptions);

  legOptions.position.z = -40;
  const leg5 = getCylinder(legOptions);

  const group = new THREE.Group();
  group.add(topSupport, leg, leg2, middleLeg, leg4, leg5);

  return group;
};
