import {
  getBoxMesh,
  addShape,
  getRectLight,
} from "../../shared/helpers/threejs";
import * as THREE from "three";

export const drawBed = () => {
  const supportOptions = {
    size: { x: 32, y: 2.6, z: 20 },
    position: { x: -32, y: 1.56, z: 38 },
    options: { color: 0xcccccc },
    shadow: { castShadow: true, receiveShadow: true },
  };
  const support = getBoxMesh(supportOptions);

  const supportOptions2 = {
    size: { x: 36, y: 1, z: 24 },
    position: { x: -32, y: 3.36, z: 38 },
    options: { color: 0xcccccc },
    shadow: { castShadow: true, receiveShadow: true },
  };
  const support2 = getBoxMesh(supportOptions2);

  const lightOptions = {
    scale: { x: 32, y: 1 },
    position: { x: -32, y: 2.85, z: 27.5 },
    options: { color: 0xffffff, intensity: 3 },
    rotation: { x: -Math.PI / 2, y: 0, z: 0 },
  };
  const light1 = getRectLight(lightOptions);

  lightOptions.position.z = 48.5;
  const light2 = getRectLight(lightOptions);

  const lightOptions2 = {
    scale: { x: 1, y: 22 },
    position: { x: -15.5, y: 2.85, z: 38 },
    options: { color: 0xffffff, intensity: 3 },
    rotation: { x: -Math.PI / 2, y: 0, z: 0 },
  };
  const light3 = getRectLight(lightOptions2);

  lightOptions2.position.x = -48.5;
  const light4 = getRectLight(lightOptions2);

  const bedOptions = {
    scale: { x: 2, y: 3, z: 0.6 },
    position: { x: -47, y: 5, z: 28 },
    options: { color: 0xffffff },
    rotation: { x: -Math.PI / 2, y: 0, z: -Math.PI / 2 },
    shadow: { castShadow: true, receiveShadow: true },
  };
  const bedShape = new THREE.Shape().lineTo(10, 0).lineTo(10, 10).lineTo(0, 10);
  const bed = addShape(bedOptions, bedShape);

  const group = new THREE.Group();
  group.add(support, support2, bed, light1, light2, light3, light4);

  return group;
};
