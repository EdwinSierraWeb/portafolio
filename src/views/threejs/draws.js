export * from "./walls";
export * from "./bed";
export * from "./desk";
export * from "./shelf";
export * from "./screen";
export * from "./keyboard";
export * from "./lamp";
export * from "./trashcan";
export * from "./lights";
export * from "./painting";

