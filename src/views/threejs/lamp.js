import {
  addShape,
  getRoundedBoxShape,
  getSphere,
} from "../../shared/helpers/threejs";
import * as THREE from "three";

export const drawLamp = () => {
  const lampSupportOptions = {
    scale: { x: 1, y: 1, z: 0.04 },
    position: { x: -47.3, y: 12.4, z: -29.8 },
    options: { color: 0xffffff },
    rotation: { x: -Math.PI / 2, y: 0, z: 0 },
    bevelEnabled: false,
    shadow: { castShadow: true, receiveShadow: true },
  };
  const lampSupportShape = getRoundedBoxShape(1.1, 1.1, 1.1, 1.1);
  const lampSupport = addShape(lampSupportOptions, lampSupportShape);

  const vectors = [
    new THREE.Vector3(0, 0),
    new THREE.Vector3(0, 1),
    new THREE.Vector3(-2, 4),
    new THREE.Vector3(0, 6),
    new THREE.Vector3(3, 6),
    new THREE.Vector3(3, 5),
  ];
  const material = new THREE.MeshStandardMaterial({ color: 0xffffff });
  const geometry = new THREE.TubeGeometry(
    new THREE.CatmullRomCurve3(vectors),
    518, // path segments
    0.2, // THICKNESS
    8, //Roundness of Tube
    false //closed
  );
  const line = new THREE.Line(geometry, material);
  line.position.set(-47.7, 12.72, -31.5);

  // const lightSupportOptions = {
  //   geometyOptions: { radius: 1.3, detail: 0 },
  //   position: { x: -44.7, y: 16.7, z: -31.5 },
  //   options: { color: 0xffffff },
  //   rotation: { x: 0, y: 0, z: -1 },
  //   shadow: { castShadow: true, receiveShadow: true },
  // };
  // const lightSupport = getDodecahedron(lightSupportOptions);

  const connectorOptions = {
    sphereOptions: {
      radius: 1.26,
      widthSegments: 30,
      heightSegments: 30,
      thetaLength: 2,
    },
    position: { x: -44.7, y: 16.47, z: -31.5 },
    options: { color: 0xffffff },
    rotation: { x: 0, y: -0.35, z: Math.PI / 2 },
  };
  const connector1 = getSphere(connectorOptions);

  const light = new THREE.PointLight(0xffffff, 3, 300);
  light.position.set(-44.7, 16.47, -31.5);

  const group = new THREE.Group();
  group.add(lampSupport, line, connector1, light);

  return group;
};
