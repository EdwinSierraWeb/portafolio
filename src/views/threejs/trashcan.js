import {
  getBoxMesh,
  addShape,
  getRectLight,
  getRoundedBoxShape,
  getCylinder,
  getTorus,
  getRing,
  getCircle,
} from "../../shared/helpers/threejs";
import * as THREE from "three";

export const drawTrashcan = () => {
  const bodyOptions = {
    cylindrerOptions: {
      radiusTop: 3.1,
      radiusBottom: 3.1,
      height: 7,
      segments: 48,
      openEnded: true,
    },
    position: { x: 5, y: 3.75, z: -44 },
    options: { color: 0xffffff, side: THREE.DoubleSide },
    shadow: { castShadow: true, receiveShadow: true },
  };
  const body = getCylinder(bodyOptions);

  const ringOptions = {
    ringOptions: {
      innerRadius: 2.1,
      outerRadius: 3.1,
      thetaSegments: 48,
    },
    position: { x: 5, y: 7.25, z: -44 },
    options: { color: 0xffffff, side: THREE.DoubleSide },
    rotation: { x: -Math.PI / 2, y: 0, z: 0 },
  };
  const topRing = getRing(ringOptions);

  const supportOptions = {
    circleOptions: {
      radius: 3.1,
      segments: 48,
    },
    position: { x: 5, y: 0.3, z: -44 },
    options: { color: 0xeeeeee, side: THREE.DoubleSide },
    rotation: { x: -Math.PI / 2, y: 0, z: 0 },
  };
  const support = getCircle(supportOptions);

  const group = new THREE.Group();
  group.add(body, topRing, support);

  return group;
};
