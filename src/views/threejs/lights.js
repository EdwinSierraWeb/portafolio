import * as THREE from "three";

const addLight = (hexColor, power = 1, distance = 100) => {
  const sphereGeometry = new THREE.SphereGeometry(0.1, 32, 16);
  const material = new THREE.MeshToonMaterial({ color: hexColor });
  const mesh = new THREE.Mesh(sphereGeometry, material);

  const light = new THREE.PointLight(hexColor, power, distance);
  light.add(mesh);

  return light;
};

export const drawLights = () => {
  const lightsOriginalPosition = {};
  const lights = [];
  for (let index = 0; index < 15; index++) {
    const light = addLight(0xffffff);

    light.userData = {
      speedX: getRandomSpeed(),
      speedY: getRandomSpeed(),
      speedZ: getRandomSpeed(),
    };

    lightsOriginalPosition[index] = {
      x: 40,
      y: getRandomPositionY(),
      z: getRandomPositionZ(),
      number: Math.random() + 3,
    };
    lights.push(light);
  }

  const groupLights = new THREE.Group();
  groupLights.add(...lights);

  const animateLights = () => {
    const time = performance.now() / 1000;
    const lightTime = time * 0.9;

    lights.forEach((light, index) => {
      const { speedX, speedY, speedZ } = light.userData;
      const { x, y, z, number } = lightsOriginalPosition[index];

      light.position.x = Math.sin(lightTime * speedX) * number + x;
      light.position.y = Math.cos(lightTime * speedY) * number + y;
      light.position.z = Math.cos(lightTime * speedZ) * number + z;
    });
  };

  return { groupLights, animateLights };
};

const getRandomSpeed = () => {
  return Math.random() * (0.7 - 0.3) + 0.3;
};

const getRandomPositionY = () => {
  return Math.random() * 50;
};

const getRandomPositionZ = () => {
  return Math.random() * (50 - -50) + -50;
};
