import {
  getBoxMesh,
  addShape,
  getRectLight,
} from "../../shared/helpers/threejs";
import { tilesTexture } from "./textures.js";
import * as THREE from "three";

export const drawShelf = () => {
  const backSupportOptions = {
    scale: { x: 0.2, y: 5, z: 0.2 },
    position: { x: 50, y: 0.28, z: -50 },
    options: { map: tilesTexture },
    rotation: { x: 0, y: -Math.PI / 2, z: 0 },
    bevelEnabled: false,
    shadow: { castShadow: true, receiveShadow: true },
  };
  const backSupportShape = new THREE.Shape()
    .lineTo(205, 0)
    .lineTo(205, 6.5)
    .lineTo(295, 6.5)
    .lineTo(295, 0)
    .lineTo(500, 0)
    .lineTo(500, 10.75)
    .lineTo(0, 10.75);

  const backSupport = addShape(backSupportOptions, backSupportShape);

  const stickOptions = {
    size: { x: 1.2, y: 1, z: 27.2 },
    position: { x: 47.82, y: 28.1, z: -30.45 },
    options: { color: 0x000000 },
    shadow: { castShadow: true, receiveShadow: true },
    rotation: { x: 0.1, y: 0, z: 0 },
  };
  const stickHorizontal1 = getBoxMesh(stickOptions);

  stickOptions.position.y = 45.903;
  stickOptions.position.z = -29.0315;
  const stickHorizontal2 = getBoxMesh(stickOptions);

  stickOptions.size.y = 18;
  stickOptions.size.z = 1;
  stickOptions.position.y = 37.902;
  stickOptions.position.z = -43;
  const stickVertical1 = getBoxMesh(stickOptions);

  stickOptions.position.y = 36.101;
  stickOptions.position.z = -16.4812;
  const stickVertical2 = getBoxMesh(stickOptions);

  const mirrorOptions = {
    size: { x: 0.5, y: 17, z: 26 },
    position: { x: 47.82, y: 37, z: -29.5 },
    options: { color: 0xffffff, roughness: 0, metalness: 0 },
    rotation: { x: 0.1, y: 0, z: 0 },
  };
  const mirror = getBoxMesh(mirrorOptions);

  const shelfOptions1 = {
    size: { x: 5, y: 1, z: 31 },
    position: { x: 45.92, y: 7.78, z: -29.5 },
    options: { map: tilesTexture },
    shadow: { castShadow: true, receiveShadow: true },
  };
  const shelf1 = getBoxMesh(shelfOptions1);

  shelfOptions1.position.y = 14.78;
  const shelf2 = getBoxMesh(shelfOptions1);

  shelfOptions1.position.y = 21.78;
  const shelf3 = getBoxMesh(shelfOptions1);

  // shelfOptions1.position.y = 28.78;
  // const shelf4 = getBoxMesh(shelfOptions1);

  // shelfOptions1.position.y = 35.78;
  // const shelf5 = getBoxMesh(shelfOptions1);

  // shelfOptions1.position.y = 42.78;
  // const shelf6 = getBoxMesh(shelfOptions1);

  // shelfOptions1.position.y = 49.78;
  // const shelf7 = getBoxMesh(shelfOptions1);

  shelfOptions1.position.z = 29.5;
  shelfOptions1.position.y = 7.78;
  const shelf8 = getBoxMesh(shelfOptions1);

  shelfOptions1.position.y = 14.78;
  const shelf9 = getBoxMesh(shelfOptions1);

  shelfOptions1.position.y = 21.78;
  const shelf10 = getBoxMesh(shelfOptions1);

  shelfOptions1.position.y = 28.78;
  const shelf11 = getBoxMesh(shelfOptions1);

  shelfOptions1.position.y = 35.78;
  const shelf12 = getBoxMesh(shelfOptions1);

  shelfOptions1.position.y = 42.78;
  const shelf13 = getBoxMesh(shelfOptions1);

  shelfOptions1.position.y = 49.78;
  const shelf14 = getBoxMesh(shelfOptions1);

  const shelfCeilingOptions = {
    size: { x: 6.6, y: 2, z: 100 },
    position: { x: 46.72, y: 55, z: 0 },
    options: { map: tilesTexture },
    shadow: { castShadow: true, receiveShadow: true },
  };
  const shelfCeiling = getBoxMesh(shelfCeilingOptions);

  // shelfOptions1.position.z = 0;
  // shelfOptions1.size.z = 80;
  // shelfOptions1.position.y = 115;
  // const shelf11 = getBoxMesh(shelfOptions1);

  // shelfOptions1.position.y = 155;
  // const shelf12 = getBoxMesh(shelfOptions1);

  // shelfOptions1.size.z = 400;
  // shelfOptions1.position.y = 199;
  // shelfOptions1.size.y = 10;
  // const shelf13 = getBoxMesh(shelfOptions1);

  const stickOptions1 = {
    size: { x: 5, y: 53.79, z: 5 },
    position: { x: 45.92, y: 27.14, z: -47.5 },
    options: { map: tilesTexture },
    shadow: { castShadow: true, receiveShadow: true },
  };

  const stick1 = getBoxMesh(stickOptions1);

  stickOptions1.position.z = -11.51;
  const stick2 = getBoxMesh(stickOptions1);

  stickOptions1.position.z = 11.51;
  const stick3 = getBoxMesh(stickOptions1);

  stickOptions1.position.z = 47.5;
  const stick4 = getBoxMesh(stickOptions1);

  const group = new THREE.Group();
  group.add(
    shelf1,
    shelf2,
    shelf3,
    shelf8,
    shelf9,
    shelf10,
    shelf11,
    shelf12,
    shelf13,
    shelf14,
    stick1,
    stick2,
    stick3,
    stick4,
    backSupport,
    stickHorizontal1,
    stickHorizontal2,
    stickVertical1,
    stickVertical2,
    mirror,
    shelfCeiling
  );

  return group;
};
