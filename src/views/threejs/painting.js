import { getBoxMesh, getRectLight } from "../../shared/helpers/threejs";
import { tilesTexture, marbleTexture } from "./textures.js";
import * as THREE from "three";

export const drawPainting = () => {
  const screenOptions = {
    size: { x: 17, y: 30, z: 0 },
    position: { x: 10, y: 34.5, z: 49.95 },
    options: { color: 0x000000, roughness: 0, metalness: 0 },
  };
  const screen = getBoxMesh(screenOptions);

  const stickOptions = {
    size: { x: 17, y: 1, z: 1 },
    position: { x: 10, y: 19, z: 49.95 },
    options: { color: 0xffffff },
  };
  const stickHorizontal1 = getBoxMesh(stickOptions);

  stickOptions.position.y = 50;
  const stickHorizontal2 = getBoxMesh(stickOptions);

  const stickOptions2 = {
    size: { x: 1, y: 32, z: 1 },
    position: { x: 1, y: 34.5, z: 49.95 },
    options: { color: 0xffffff },
  };
  const stickVertical1 = getBoxMesh(stickOptions2);

  stickOptions2.position.x = 19;
  const stickVertical2 = getBoxMesh(stickOptions2);

  const group = new THREE.Group();
  group.add(
    screen,
    stickHorizontal1,
    stickHorizontal2,
    stickVertical1,
    stickVertical2
  );

  return group;
};
