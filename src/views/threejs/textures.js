import * as THREE from "three";

import woodImage from "../../assets/img/textures/Wood_024_basecolor.jpg";
import marbleImage from "../../assets/img/textures/Marble_Carrara_003_COLOR.jpg";
import tilesImage from "../../assets/img/textures/Wood_Wall_003_basecolor.jpg";

const textureLoader = new THREE.TextureLoader();

const woodTexture = textureLoader.load(woodImage);
const marbleTexture = textureLoader.load(marbleImage);
const tilesTexture = textureLoader.load(tilesImage);

export { woodTexture, marbleTexture, tilesTexture };
