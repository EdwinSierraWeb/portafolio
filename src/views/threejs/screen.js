import {
  getBoxMesh,
  addShape,
  getRectLight,
  getRoundedBoxShape,
} from "../../shared/helpers/threejs";
import * as THREE from "three";

export const drawScreen = () => {
  const supportOptions = {
    size: { x: 2, y: 0.2, z: 3 },
    position: { x: -19, y: 12.52, z: -46.6 },
    options: { color: 0xffffff },
    shadow: { castShadow: true, receiveShadow: true },
  };
  const support1 = getBoxMesh(supportOptions);

  const supportOptions2 = {
    scale: { x: 0.67, y: 1, z: 0.03 },
    position: { x: -19.35, y: 12.54, z: -47.2 },
    options: { color: 0xffffff },
    rotation: { x: 0.1, y: 0, z: 0 },
    bevelEnabled: false,
    shadow: { castShadow: true, receiveShadow: true },
  };
  const supportShape2 = getRoundedBoxShape(1, 3, 1, 1);

  const support2 = addShape(supportOptions2, supportShape2);

  const supportOptions3 = {
    scale: { x: 1, y: 1, z: 0.245 },
    position: { x: -18.03, y: 17.5, z: -46.51 },
    options: { color: 0xffffff },
    rotation: { x: 0, y: -Math.PI / 2, z: 0 },
    bevelEnabled: false,
    shadow: { castShadow: true, receiveShadow: true },
  };
  const supportShape3 = new THREE.Shape()
    .quadraticCurveTo(-0.2, 0, -0.2, -0.2)
    .quadraticCurveTo(-0.2, -0.4, 0, -0.4)
    .quadraticCurveTo(0.2, -0.4, 0.2, -0.2)
    .quadraticCurveTo(0.2, 0, 0, 0);
  const support3 = addShape(supportOptions3, supportShape3);

  const screenSupportOptions = {
    scale: { x: 1, y: 1, z: 0.02 },
    position: { x: -24.03, y: 14.5, z: -46.06 },
    options: { color: 0xffffff },
    rotation: { x: -0.1, y: 0, z: 0 },
    bevelEnabled: false,
    shadow: { castShadow: true, receiveShadow: true },
  };
  const screenSupportShape = getRoundedBoxShape(10, 6, 0.5, 0.5);

  const screenSupport = addShape(screenSupportOptions, screenSupportShape);

  const screenOptions = {
    scale: { x: 10.3, y: 5.35 },
    position: { x: -19.03, y: 18.5, z: -46.25 },
    options: { color: 0xffffff, intensity: 15 }, //3
    rotation: { x: -0.1, y: -Math.PI, z: 0 },
  };
  const screen = getRectLight(screenOptions);

  const group = new THREE.Group();
  group.add(support1, support2, support3, screenSupport, screen);

  return group;
};
