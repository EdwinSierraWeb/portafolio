export const ARROW_KEYS = {
  ArrowRight: "right",
  ArrowLeft: "left",
  ArrowUp: "top",
  ArrowDown: "bottom",
};

let seaColorOffset = 0; // Offset for changing sea color
let seaWaveOffset = 0; // Offset for simulating sea waves

export const CUBE_SIZE = {
  X: 40,
  Y: 40,
};

export const setCanvasSize = ({ canvas }) => {
  canvas.width = getCanvasWidth();
  canvas.height = getCanvasHeight();
};

export const getCanvasWidth = () => window.innerWidth;

export const getCanvasHeight = () => window.innerHeight;

export const drawSeaColumn = (context, position) => {
  position.y = 0;

  while (position.y < getCanvasHeight()) {
    drawSea(context, position);

    position.y += 40;
  }
};

const drawSea = (context, { x, y }) => {
  context.fillStyle = "rgb(29, 162, 216)";
  context.fillRect(x, y, CUBE_SIZE.X, CUBE_SIZE.Y);

  context.strokeStyle = "rgb(6, 66, 115)";
  context.lineWidth = 0.3;
  context.strokeRect(x, y, CUBE_SIZE.X, CUBE_SIZE.Y);
};

export const drawLawn = (context, { x, y }) => {
  context.fillStyle = "rgb(124, 252, 0)";
  context.fillRect(x, y, CUBE_SIZE.X, CUBE_SIZE.Y);

  context.strokeStyle = "rgb(85, 169, 0)";
  context.lineWidth = 0.3;
  context.strokeRect(x, y, CUBE_SIZE.X, CUBE_SIZE.Y);
};
