export const getExperienceContent = () => {
  const content = document.createElement("article");
  content.className =
    "w-auto h-auto col-xxl-8 p-md-3 px-1 py-3 shadow bg-white text-dark";

  const innerContent = `
      <section>
        <header>
          <h6 class="fw-bold">RESPONSIBILITIES</h6>
        </header>
  
        <ul>
          <li>Analyze and carry out software requirements.</li>
          <li>Engineering initiative to improve performance, reduce dependencies, and enhance the development and operational experience.</li>
          <li>Support coworkers with code doubts and business logic.</li>
          <li>Promote engineering best practices and quality software by helping all the people, reviewing code, and giving feedback.</li>
          <li>Carry out Front-end and Back-end development.</li>
          <li>Main company's Front-end developer.</li>
          <li>Main company's Back-end (NodeJs) developer.</li>
          <li>Debugging.</li>
          <li>Test mobile application.</li>
        </ul>
      </section>
  
      <section>
        <header>
          <h6 class="fw-bold">KEY ACCOMPLISHMENTS</h6>
        </header>
  
        <ul class="m-0">
          <li>Helped to manage company's information by developing a Back office project (Front end).</li>
          <li>Avoided Front-end deprecation by migrating from Vue 2 to Vue 3.</li>
          <li>Improved efficiency, effectiveness, and performance by creating a Route Reengineering module.</li>
          <li>Improved the company's payments by creating Credits and Collections module.</li>
          <li>Reduced deprecated reports and increased download time by 96% by migrating from Java to NodeJs, removing Java's API.</li>
          <li>Avoided project's API deprecation by helping to migrate from Ruby v2 to v3 and Rails v4 to v7.</li>
          <li>Enhanced reports download efficiency and reduced data overhead through Streamlining Techniques.</li>
          <li>Improved Base components efficiency by migrating them to better ones.</li>
          <li>Avoided map's deprecation by migrating them (Google Maps API).</li>
          <li>Improved Liquidation's views to see Credits and Collections by handling them by visits and orders.</li>
          <li>Increased route efficiency by creating route indicators with times, sales, productivity, and services.</li>
          <li>Increased repository efficiency by standardizing components.</li>
          <li>Enabled Visits indicator by creating a Visits module.</li>
          <li>Created other modules: Decline Causes, Surveys, Notifications, and Notification Types.</li>
        </ul>
      </section>
    `;

  content.innerHTML = innerContent;

  return content;
};
