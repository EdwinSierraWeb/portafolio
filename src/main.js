import { createApp } from "vue";
import App from "./App.vue";
import router from "./router/index.js";
import i18n from "./i18n"

import "./vendor.js";


const app = createApp(App);

app.use(i18n);
app.use(router);

app.mount("#app");

import "bootstrap/dist/js/bootstrap.js";
